/**
 * we use require directive to load node.js modules
 * module is a software component or a part of a program that contains one or more routines
 * http module has node.js transfer data using hyper text transfer protocol
 * http module is a set of individual files that contain code to create a component that helps establish data transfer between applications
 * 
 * HTTP is a protocol that allows fetching of resources such html documents
 * 
 * client browser and server nodejs/expressjs
 * 
 * clients - request
 * server - response
 */


let http = require("http");
/**
 * createServer() -found inside http module; a method that accepts a function as its argument for a creation of a server.
 * request, response - arguments that are passed to the createServer() method; this would allow us to receive requests 1st paramater and send responses 2nd parameter
 * 
 * listen(port) allows our application to be run in our local devices through a specified port.
 * 
 * port - 0-99999
 */

http.createServer(function(request,response){
    response.writeHead(200,{"Content-Type":"text/plain"});



    response.end("hello World");
    

}).listen(4000);
console.log("Server Running at port: 4000");

/**
 * nodemon
 * install this package to automatically restart when files have been changed
 * npm install -g nodemon
 * -g means that we are going to install the package globally in our device.
 */