const http = require("http");

//storing the port 4000 in a variable called port
const port = 4000;

// storing the createServer method inside the server
const server = http.createServer((request,response) => {
    if(request.url === "/greeting"){
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.end("hello World");
    }else if(request.url === "/homepage"){
        response.writeHead(200,{"Content-Type":"text/plain"});
        response.end("Hello");
    }else{
        response.writeHead(404,{"Content-Type":"text/plain"});
        response.end("Page not found!");
    }
});

server.listen(port);
console.log(`Server now running at port: ${port}`);